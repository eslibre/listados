---
layout: post
section: associations
title: Asociación de Estudiantes de Software Libre de la Universidad Pablo de Olavide
---

## Información

-   Universidad a la que está asociada: Universidad Pablo de Olavide
-   Activa en la actualidad: Sí

## Contacto

-   Sede física: Universidad Pablo de Olavide. Carretera de Utrera Km 1, 41013 Sevilla, Sevilla
-   Página web: <https://esoliupo.es/>
-   Email: <info@esoliupo.es>
-   Twitter: <https://twitter.com/esoliupo>
-   Grupo o canal en Telegram: <https://t.me/asocesoliupo>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/Asociacion-ESOLIUPO/>
