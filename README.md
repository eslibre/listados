# Listados de organizaciones

En esLibre intentamos mantener un evento anual donde todas las personas estén invitadas a participar, lo único que se necesita es tener algo que decir sobre el mundo de las tecnologías y cultura libre. Nos interesan personas de todas las edades, experiencias y perfiles ya sean técnicas o usuarias.

Estamos deseando escuchar y aprender de todas las personas y nuestro objetivo principal es llegar a comunidades de todo tipo. Por eso, para intentar llegar a tantas comunidad como sea posible y tenerlas en cuenta a la hora de organizar eventos, queremos crear un listado con diferentes tipos de organizaciones que tienen como objetivo la divulgación y defensa del [software libre](https://www.gnu.org/philosophy/free-sw.html), [hardware libre](https://es.wikipedia.org/wiki/Hardware_libre) o la [cultura libre](https://freedomdefined.org/Definition/Es).

-   [Comunidades con apoyo explícito a las tecnologías y cultura libres](comunidades/)
-   [Oficinas de Software Libre en las Universidades](oficinas-sl/)
-   [Asociaciones Estudiantiles Universitarias Libres](asoc-universitarias/)
-   [Otras comunidades generales tecnológicas/culturales](otras/)\*

(\*Cualquier comunidad que se dedique a la divulgación tecnológica/cultural y que no esté asociada a tecnologías privativas o intereses comerciales)

¿Cónoces alguna que no esté listada? Haznos un [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) a este repositorio o escríbenos a <mailto:hola@eslib.re>.
