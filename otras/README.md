# Otras comunidades generales

En <https://listados.eslib.re/otras/> puedes encontrar listadas las páginas con información de las siguientes comunidades que, aunque no se enfoquen en las tecnologías o el conocimiento libre, se dedican a la divulgación tecnológica/cultural y no están asociada a tecnologías privativas o intereses comerciales:

-   [Alhambra Makers](alhambra-makers.md)
-   [Andalucía Developers](andalucia-developers.md)
-   [AndaluGeeks](andalugeeks.md)
-   [Barcelona Perl Mongers](barcelona-pm.md)
-   [Club Robótica Granada](club-robotica-granada.md)
-   [Fundació Guifi.net](guifi-net.md)
-   [HackLab Almería](hacklab-almeria.md)
-   [HackMadrid%27](hackmadrid.md)
-   [HaskellMAD](haskell-mad.md)
-   [Ingeniería Sin Fronteras - Andalucía](isf-andalucia.md)
-   [La Jaquería](la-jaqueria.md)
-   [LibreIM](libreim.md)
-   [MakeSpace Madrid](makespace-madrid.md)
-   [Malaga JUG](malaga-jug.md)
-   [Medialab Prado](medialab-prado.md)
-   [Ondula](ondula.md)
-   [OpenExpo - Europe](openexpo.md)
-   [Panoptikuhn](panoptikuhn.md)
-   [Ping a Programadoras](ping-programadoras.md)
-   [PyLadies Madrid](pyladies-madrid.md)
-   [Python España](python-españa.md)
-   [R-Ladies Madrid](rladies-madrid.md)
-   [Recuncho Maker](Recuncho Maker.md)
-   [Sevilla Maker Society](sevilla-maker.md)
-   [TheThingsNetwork-MAD](ttn-mad.md)
-   [Trackula](trackula.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/otras.md).
